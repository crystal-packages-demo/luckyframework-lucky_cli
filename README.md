# luckyframework-lucky_cli

[luckyframework/lucky_cli](https://github.com/luckyframework/lucky_cli) creating and running tasks. [luckyframework.org](https://luckyframework.org)

# Ask questions
* https://discord.gg/HeqJUcb

# Unofficial documentation
* [*Lucky Tutorial - Let's build a Bitcoin wallet*
  ](https://onchain.io/blog/lucky_tutorial)
  ONCHAIN.IO
